﻿using PERSON_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PERSON_API.Services.Interfaces
{
    public interface IPersonService
    {
        List<PersonDto> GetAllPerson();
        void AddPerson(PersonDto person);
    }
}
