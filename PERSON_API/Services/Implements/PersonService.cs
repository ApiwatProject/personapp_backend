﻿using PERSON_API.Models;
using PERSON_API.Repositories.Interface;
using PERSON_API.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PERSON_API.Services.Implements
{
    public class PersonService : IPersonService
    {
        private readonly IPersonRepository personRepository;
        public PersonService(IPersonRepository personRepository)
        {
            this.personRepository = personRepository;
        }
        public void AddPerson(PersonDto person)
        {
            if(String.IsNullOrEmpty(person.UserName) || String.IsNullOrEmpty(person.UserSurname))
            {
                throw new ArgumentException("Username and/or UserSurname is required");
            }
            else
            {
                personRepository.AddPerson(person);
            }
        }

        public List<PersonDto> GetAllPerson()
        {
            List<PersonDto> personList = personRepository.GetAllPerson();
            return personList;
        }
    }
}
