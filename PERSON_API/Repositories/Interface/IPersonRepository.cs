﻿using PERSON_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PERSON_API.Repositories.Interface
{
    public interface IPersonRepository
    {
        void AddPerson(PersonDto person);
        List<PersonDto> GetAllPerson();

    }
}
