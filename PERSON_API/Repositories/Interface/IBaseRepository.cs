﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PERSON_API.Repositories.Interface
{
    public interface IBaseRepository
    {
        IEnumerable<T> QueryString<T>(string sql);

        int ExecuteString<T>(string sql);
    }
}
