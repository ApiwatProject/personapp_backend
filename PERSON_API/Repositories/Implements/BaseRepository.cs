﻿using Dapper;
using PERSON_API.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace PERSON_API.Repositories.Implements
{
    public class BaseRepository : IBaseRepository
    {
        private readonly string _ConnectionString;

        public BaseRepository(string ConnectionString)
        {
            this._ConnectionString = ConnectionString;
        }

        //Method Connect to Sql Server
        private T WithConnection<T>(Func<IDbConnection, T> getData)
        {
            try
            {
                using var connection = new SqlConnection(_ConnectionString);
                connection.Open();
                return getData(connection);
            }
            catch (SqlException e)
            {

                throw e;
            }
        }
        public IEnumerable<T> QueryString<T>(string sql)
        {
            return WithConnection(c => c.Query<T>(sql));
        }

        public int ExecuteString<T>(string sql)
        {
            return WithConnection(c => c.Execute(sql));
        }
    }
}
