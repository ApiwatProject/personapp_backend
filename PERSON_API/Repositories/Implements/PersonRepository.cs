﻿using PERSON_API.Models;
using PERSON_API.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PERSON_API.Repositories.Implements
{
    public class PersonRepository : IPersonRepository
    {
        private readonly IBaseRepository baseRepository;
        public PersonRepository(IBaseRepository baseRepository)
        {
            this.baseRepository = baseRepository;
        }
        public void AddPerson(PersonDto person)
        {

            string sqlScript = $@"INSERT INTO [dbo].[USER](
                                            [UserName]
                                           ,[UserSurname])
                                     VALUES
                                           ('{person.UserName}'
                                           ,'{person.UserSurname}')";
            int insertResult = baseRepository.ExecuteString<int>(sqlScript);
            if(insertResult == 0)
            {
                throw new Exception("Cannot Insert data right now");
            }
        }

        public List<PersonDto> GetAllPerson()
        {
            string sqlScript = $@"SELECT * FROM [dbo].[USER]";
            List<PersonDto> personList =  baseRepository.QueryString<PersonDto>(sqlScript).ToList();
            return personList;
        }

    }
}
