﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PERSON_API.Models.Output
{
    public class ResponseHeaderDto
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public object Content { get; set; }

        public ResponseHeaderDto(string Status, string Message, object Content)
        {
            this.Status = Status;
            this.Message = Message;
            this.Content = Content;
        }

        public ResponseHeaderDto() { }

       
    }
}
