﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PERSON_API.Models;
using PERSON_API.Models.Output;
using PERSON_API.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PERSON_API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PersonController : ControllerBase
    {
        private readonly IPersonService personService;
        public PersonController(IPersonService personService)
        {
            this.personService = personService;
        }
        [HttpPost]
        public IActionResult AddPerson( [FromBody]PersonDto person )
        {
            try
            {
                personService.AddPerson(person);
                return StatusCode(StatusCodes.Status200OK, new ResponseHeaderDto("S", "Add Person Successful" , null));
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new ResponseHeaderDto("F", e.Message, e.StackTrace));
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,new ResponseHeaderDto("F" , e.Message,e.StackTrace));
            }
        }

        public IActionResult GetAllPerson()
        {
            try
            {
                List<PersonDto> personList = personService.GetAllPerson();
                return StatusCode(StatusCodes.Status200OK, new ResponseHeaderDto("S", "Get Person Successful", personList));
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new ResponseHeaderDto("F", e.Message, e.StackTrace));
            }
        }
    }
}
